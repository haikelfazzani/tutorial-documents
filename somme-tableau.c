#include <stdio.h>
#include <string.h>

// fonction qui calcul somme d'un tableau
int somme(int tab[] , int len) {
    int somme = 0;
    for(int i = 0 ; i < len ; i++) {
        somme += tab[i];
    }
    return somme;
}

int main()
{
    int tab[] = {1,2,3};
    // calcul longueur d'un tableau
    int len = sizeof(tab)/sizeof(int);

    int som = somme(tab , len);
    
    printf("somme est : %d" , som);
}