#include <stdio.h>

int main()
{  
    int nbLigne = 0;

    printf("entrer nombre de lignes : ");
    scanf("%d" , &nbLigne);

    for(int i = 1 ; i <= nbLigne ; i++) {
        for(int j = 1 ; j <= i ; j++) {
            printf(" * ");
        }
        printf("\n");
    }
}