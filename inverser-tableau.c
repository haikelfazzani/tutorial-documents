// https://pastebin.com/EUzTG8S7
#include <stdio.h>
 
int main()
{
  // lire taille du tableau entrer par l'utilisateur
  int taille;
  printf("entrer taille d un tableau : ");
  scanf("%d" , &taille);
 
// lire les valeurs entrer des l'utilisateur
  int tab[taille];
  for(int i = 0 ; i < taille ; i++) {
      printf("entrer un valeur : ");
      scanf("%d" , &tab[i]);
  }
 
// inverser les éléments du tableau
  for(int i = 0 ; i < taille/2 ; i++) {
        int temp = tab[i];
        tab[i] = tab[taille - i - 1]; // 1 4
        tab[taille - i - 1 ] = temp;
  }
 
// afficher les éléments du tableau
  for(int i = 0 ; i < taille ; i++) {
      printf("%d \n" , tab[i]);
  }
}